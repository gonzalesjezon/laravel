<link rel="stylesheet" href="{{ URL::asset('beagle-assets/css/app.css') }}" type="text/css"/>
<div id="reports" style="width: 960px;margin: auto; font-size: 14px;font-family: Arial, Helvetica, sans-serif;">
	<div class="row form-group">
		<div class="col-6">
			<img src="{{ asset('img/pcc-logo-small.png')}}">
		</div>
		<div class="col-6 text-right">
			<p class="p-0 m-0">25/F Vertis North Corporate Center 1,</p>
			<p class="p-0 m-0">North Avenue, Quezon City 1105</p>
			<p class="p-0 m-0">querys@phcc.gov.ph</p>
			<p class="p-0 m-0">(+632)7719-PCC (7719-722)</p>
		</div>
	</div>
	<hr>

	<div class="row form-group">
		<div class="col-6"></div>
		<div class="col-6">{{ date('F d, Y',time()) }}</div>
	</div>

	<div class="row form-group">
		<div class="col-6">
			<p class="p-0 m-0 font-weight-bold text-uppercase">{{ $data->getFullName() }}</p>
			<p class="p-0 m-0">{{ $data->house_number }} {{ $data->street }}</p>
			<p class="p-0 m-0">{{ $data->subdivision }}</p>
			<p class="p-0 m-0 mb-7">{{ $data->city }}</p>
			<p >Dear <strong>Mr/Ms. {{$data->last_name}}</strong>,</p>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-12 text-center">
			<strong class="text-uppercase">RE: <u>ATTESTATION OF APPOINTMENT</u></strong>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<p class="mb-6">Congratualations!</p>
			<p class="text-justify">
				We are pleased to inform you that the Civil Service Commission has approved your attestation to the  <b>{{ $data->job->psipop->position_title }}</b> position, JG- {{ $data->job->psipop->salary_grade }}, Item No. {{ $data->job->psipop->item_number }}, {{ config('params.employee_status.'.$data->job->psipop->employee_status)}} under the <b>{{ $data->job->psipop->division->name }}</b>. The annual compensation package is broken down as follows:
			</p>
		</div>
	</div>

	<div class="row mb-4">
		<div class="col-12">

			<p style="text-indent: 3em;">
				A copy of the attested appointment paper is attached for your reference.
			</p>

			<p style="text-indent: 3em;">
				Thank you for your continued hard work and support
			</p>
		</div>
	</div>

	<div class="row">
		<div class="col-8"></div>
		<div class="col-4">
			<p class="mb-4">Very truly yours,</p>
			<p class="mb-0 pb-0"><b>Kenneth V. Tanate</b></p>
			<p class="m-0 p-0">Executive Director</p>
		</div>
	</div>
</div>