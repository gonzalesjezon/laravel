<!-- Left Sidebar -->
<div class="be-left-sidebar">
  <div class="left-sidebar-wrapper">
    <a href="#" class="left-sidebar-toggle">Dashboard</a>
    <div class="left-sidebar-spacer">
      <div class="left-sidebar-scroll">
        <div class="left-sidebar-content">
          <ul class="sidebar-elements">
            <li class="divider">Menu</li>
            <li class="active">
              <a href="{{ route('dashboard') }}"><i class="icon mdi mdi-home"> </i><span>Dashboard</span></a>
            </li>
            @if(Auth::id() == config('params._SUPER_ADMIN_ID_'))
              <li class="parent">
                <a href="#"><i class="icon mdi mdi-settings"> </i><span>Configurations</span></a>
                <ul class="sub-menu">
                  <li><a href="{{ route('config.index') }}">List configurations</a></li>
                </ul>
              </li>
            @endif
          </ul>
        </div>
      </div>
    </div>

    <div class="progress-widget">
      <div class="progress-data"><span class="progress-value">70%</span><span class="name">Current Project</span>
      </div>
      <div class="progress">
        <div style="width: 60%;" class="progress-bar progress-bar-primary"></div>
      </div>
    </div>
  </div>
</div>
<!-- /. Left Sidebar -->
