<?php

/**
 * Public Routes
 *
 */
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/menu', 'HomeController@index')->name('menu');
Route::get('/careers', 'HomeController@careers')->name('careers');
Auth::routes();

/**
 * Routes that require user to be logged in
 * filtered by middleware auth in controller
 */
Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');


Route::resources([
	'config' => 'ConfigController',
]);

