<?php

use Illuminate\Database\Seeder;
use App\Applicant,
    Carbon\Carbon;
    
class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Applicant::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Applicant::create([
                'company_id' => rand(1,10),
                'branch_id' => rand(1,10),
                'lastname' => $faker->lastName,
                'firstname' => $faker->firstName,
                'middlename' => $faker->lastName,
                'email_address' => $faker->email,
                'birthday' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = '-20 years'),
                'mobile_number' => $faker->phoneNumber,
                'created_by' => rand(1,10),
                'updated_by' => rand(1,10),
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ]);
        }
    }
}
