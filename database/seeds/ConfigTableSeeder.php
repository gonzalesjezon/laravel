<?php

use Illuminate\Database\Seeder,
    App\Config,
    Carbon\Carbon;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Config::truncate();

        $fields = [
            'application_recipient_name'            => 'John Doe',
            'application_recipient_title'           => 'Department Manager',
            'application_recipient_department'      => 'Human Resources Department',
            'application_recipient_organization'    => 'Organization Name',
            'application_recipient_address'         => '123 street corner ABC avenue, Philippines',
            'application_requirements'              => 'Letter of Application, Latest Personal Data Sheet',
            'url_rms'                               => 'http://rms-url-here',
            'url_pis'                               => 'http://pis-url-here',
            'url_pms'                               => 'http://pms-url-here',
            'url_ams'                               => 'http://ams-url-here',
            'url_ldms'                              => 'http://ldms-url-here',
            'url_spms'                              => 'http://spms-url-here',
        ];

        // And now, let's create a few articles in our database:
        foreach ($fields as $key => $value) {
            Config::create([
                'name' => $key,
                'value' => $value,
                'created_by' => 1,
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' =>Carbon::now()->toDateTimeString(),
            ]);
        }
    }
}
