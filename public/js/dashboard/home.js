var Webapp = {
  charts: function() {

    // bar chart for dashboard
    new Chart($('#plantilla-chart'), {
      type: 'bar',
      data: {
        labels: ['Plantilla', '', '', 'Non-Plantilla', '', ''],
        datasets: [
          {
            label: ['Plantilla'],
            data: [30, 10, 10, 40, 20, 20],
            backgroundColor: [
              'rgba(54, 162, 235, 0.2)', // blue
              'rgba(255, 206, 86, 0.2)', // Yellow
              'rgba(255, 159, 64, 0.2)', // Orange
              'rgba(75, 192, 192, 0.2)', // green
              'rgba(255, 206, 86, 0.2)', // Yellow
              'rgba(255, 159, 64, 0.2)', // Orange
            ],
            borderColor: [
              'rgba(54, 162, 235, 1)', // blue - line
              'rgba(255, 206, 86, 1)', // Yellow - line
              'rgba(255, 159, 64, 1)', // Orange - line
              'rgba(75, 192, 192, 1)', // green - line
              'rgba(255, 206, 86, 1)', // Yellow - line
              'rgba(255, 159, 64, 1)', // Orange - line
            ],
            borderWidth: 1,
          },
          {
            label: ['Non-Plantilla'],
            backgroundColor: [
              'rgba(75, 192, 192, 0.2)', // green
            ],
            borderColor: [
              'rgba(75, 192, 192, 1)', // green - line
            ],
            borderWidth: 1,
          },
          {
            label: ['Male'],
            backgroundColor: [
              'rgba(255, 206, 86, 0.2)', // Yellow
            ],
            borderColor: [
              'rgba(255, 206, 86, 1)', // Yellow - line
            ],
            borderWidth: 1,
          },
          {
            label: ['Female'],
            backgroundColor: [
              'rgba(255, 159, 64, 0.2)', // Orange
            ],
            borderColor: [
              'rgba(255, 159, 64, 1)', // Orange - line
            ],
            borderWidth: 1,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
              },
            }],
          xAxes: [
            {
              barPercentage: 2.5, // bar thickness
            }],
        },
        legend: {
          display: true,
          labels: {
            fontColor: 'rgb(255, 99, 132)',
          },
        },
      },
    });
  },
};